﻿using PsdExporter;
using SceneExporter.HydraExporter.Handlers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace SceneExporter.HydraExporter
{
    [ExportType(".xml", "Hydra_Exporter")]
    public class HydraExporter : Exporter
    {
        private static PsdObject _CurrentTopPsdObject;

        private static Int32 _ZIndex;

        private static String _ZBase;

        public static String GetZ()
        {
            return $"{_ZBase}_{_ZIndex}";
        }

        public static void IncrementZ() => _ZIndex++;

        public static ExportSettings Settings { get; private set; }

        public static XElement MainElement { get; private set; }

        public static XElement SceneElement { get; private set; }

        private static readonly List<PsdObjectHandler> _ObjectHandlers = new List<PsdObjectHandler>()
        {
            new SpriteHandler(),
            new AnchorHandler(),
            new AnimSpriteHandler(),

            new SpritesHandler(),

            //Hydra does not support custom objects
            new CustomObjectHandler(),
        };

        public static String SceneName { get; private set; }

        public static String SceneAnchorName { get; private set; }

        public override void Start(String sceneName, ExportSettings settings)
        {
            SceneName = sceneName;
            _ZBase = sceneName;
            Settings = settings;
            _ZIndex = 0;

            Settings.InferPackWrapAround = true;
            settings.TrimOutput = false;

            HydraExporter.SceneAnchorName = HydraExporter.Settings.CustomArguments["SceneAnchor"];

            MainElement = new XElement("Layout");
            /*_MainElement.Add(new XAttribute("xmlns:xsi", @"http://www.w3.org/2001/XMLSchema-instance"));
            _MainElement.Add(new XAttribute("xsi:noNamespaceSchemaLocation", "../../Hydra/src/hydra/core/xml/Layout.xsd"));*/

            SceneElement = new XElement("Scene");
            SceneElement.Add(new XAttribute("ReferenceName", sceneName));
            SceneElement.Add(new XElement("Visible", true));
        }

        public static List<PsdObject> GetAll(PsdObject psdObject, Predicate<PsdObject> pattern, Boolean removeOnMatchFromParent = false)
        {
            List<PsdObject> psdObjects = new List<PsdObject>();
            Search(psdObject, psdObjects, pattern, removeOnMatchFromParent);
            return psdObjects;
        }

        public static void Search(PsdObject psdObject, List<PsdObject> psdObjects, Predicate<PsdObject> pattern, Boolean removeOnMatchFromParent = false)
        {
            if (pattern?.Invoke(psdObject) ?? false)
            {
                psdObjects.Add(psdObject);
                if (removeOnMatchFromParent)
                {
                    psdObject.Parent?.Children.Remove(psdObject);
                }
            }

            for (Int32 i = psdObject.Children.Count - 1; i >= 0; i--)
            {
                PsdObject psdObjectChild = psdObject.Children[i];
                Search(psdObjectChild, psdObjects, pattern, removeOnMatchFromParent);
            }
        }

        public override void Process(PsdObject[] packs)
        {
            //Dont output anything for packs
            foreach (PsdObject packObject in packs)
            {
                _CurrentTopPsdObject = packObject;

                //All objects in packs need to be added

                for (Int32 i = 0; i < packObject.Children.Count; i++)
                {
                    PsdObject psdObject = packObject.Children[i];
                    //_ZBase = groupObject.Name;

                    XElement[] sceneElements = GetElements(psdObject, null);
                    if (sceneElements != null)
                    {
                        foreach (XElement sceneElement in sceneElements)
                        {
                            SceneElement.Add(sceneElement);
                        }
                        IncrementZ();
                    }
                }

                IncrementZ();
            }

            MainElement.Add(SceneElement);
        }

        public static XElement[] GetElements(PsdObject psdObject, PsdObject anchor = null)
        {
            string objectName = GetObjectName(psdObject);
            if (psdObject.Type == "Group" && !objectName.StartsWith("[$"))
            {
                //Find the first anchor inside the group, this will be the anchor used for the group, rest will be ignored
                List<PsdObject> anchors = GetAll(psdObject, o => "Anchor".Equals(o.Type), true).Where(s => s != psdObject).ToList();

                if (anchors.Any(s => s.Name.StartsWith(psdObject.Name)))
                {
                    anchor = anchors.First(s => s.Name.StartsWith(psdObject.Name));
                }
                else
                {
                    psdObject.Offset = new[] { 0, 0 };

                    anchor = new PsdObject
                    {
                        Type = "Anchor",
                        Name = $"{psdObject.Name}-Anchor",
                        Offset = new[] { psdObject.Offset[0], psdObject.Offset[1] },
                    };

                    Dictionary<string, string> anchorData = psdObject.GetData(true);
                    if (anchorData.TryGetValue("$Origin", out String anchorOrigin))
                    {
                        string[] split = anchorOrigin.Split(',');
                        anchor.Offset = new[] { int.Parse(split[0]), int.Parse(split[1]) };
                    }
                }

                //Remove the group from the hierarchy, as Anchors should be outside the scope of the object,
                //but is more confusing to make them like that in Photoshop
                anchor.Parent = psdObject.Parent;

                XElement[] anchorElements = GetElements(anchor, null);
                foreach (XElement anchorElement in anchorElements)
                {
                    MainElement.Add(anchorElement);
                }
            }

            PsdObjectHandler handler = null;
            XElement[] elements = null;

            foreach (PsdObjectHandler psdObjectHandler in _ObjectHandlers)
            {
                if (!psdObjectHandler.Create(psdObject, anchor, out elements)) continue;
                handler = psdObjectHandler;
                break;
            }

            if (elements == null)
            {
                Console.WriteLine($"No handler for {psdObject.Type}");
                return null;
            }

            foreach (XElement element in elements)
            {
                List<PsdObject> psdObjectChildren = psdObject.Children;

                if (psdObjectChildren.Any())
                {
                    //psdObjectChildren.Reverse();
                    foreach (PsdObject psdObjectChild in psdObjectChildren)
                    {
                        if (handler?.ChildAlreadyParsed(psdObjectChild) ?? false) continue;

                        XElement[] childElements = GetElements(psdObjectChild, anchor);
                        if (childElements == null) continue;

                        foreach (XElement childElement in childElements)
                        {
                            _ZIndex++;
                            element.Add(childElement);
                        }
                    }
                }
            }

            return elements;
        }

        public override String End()
        {
            return MainElement.ToString();
        }

        public override String GetOutputFilePath()
        {
            return Path.Combine($"{Settings.OutputFileName}.xml");
        }

        public static string GetObjectName(PsdObject psdObject)
        {
            string objectName = psdObject.Name;
            if (psdObject.Parent != null && Settings.PrefixParentName &&
                psdObject.Name.StartsWith(psdObject.Parent.Name))
            {
                //+1 fo the added - with the parental name prefix
                int startIndex = psdObject.Parent.Name.Length + 1;
                objectName = objectName.Substring(startIndex, objectName.Length - startIndex);
            }
            return objectName;
        }
    }
}
