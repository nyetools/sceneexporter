﻿using PsdExporter;
using System;
using System.Xml.Linq;

namespace SceneExporter.HydraExporter.Handlers
{
    public abstract class PsdObjectHandler
    {
        /// <summary>Attempt to create a <see cref="XElement"/> using the given <see cref="PsdObject"/></summary>
        /// <param name="psdObject">The data given to create an <see cref="XElement"/> from</param>
        /// <param name="psdObjectAnchor">Anchor the given object is relative to, null if there isn't one</param>
        /// <param name="element">The out result if this object can handle the creation</param>
        /// <returns>true if a <see cref="XElement"/> was created from this <see cref="PsdObject"/></returns>
        public abstract Boolean Create(PsdObject psdObject, PsdObject psdObjectAnchor, out XElement[] elements);

        /// <summary>Will be invoked for every child on a <see cref="PsdObject"/> that was just handled.
        /// Used if the Handler reads all children in one go, and will then ignore the parsing of each individual child there after.
        /// Eg. AnimSprite creates Frame Data from children immediately, does not need to process each child after that if it is a Sprite.</summary>
        /// <param name="psdObject"></param>
        /// <returns></returns>
        public abstract Boolean ChildAlreadyParsed(PsdObject psdObject);

        protected XElement GetAnchor(PsdObject psdObject, PsdObject psdObjectAnchor = null)
        {
            //Hydra doesnt have CustomObjects, do Groups are not used and therefore they do not have their own anchors
            //return new XElement("ParentAnchor", HydraExporter.SceneAnchorName);

            if (psdObject.Parent != null && psdObject.Parent.Type == "Pack" &&
                psdObject.Type != "Group")
            {
                return new XElement("ParentAnchor", HydraExporter.SceneAnchorName);
            }


            if (psdObject.Parent != null)
            {

                String anchorName = psdObjectAnchor?.Name ?? $"{psdObject.Parent.Name}-Anchor";

                if ("Pack".Equals(psdObject.Parent?.Type))
                {
                    anchorName = $"{psdObject.Name}-Anchor";
                }

                return new XElement("ParentAnchor", anchorName);
            }
            //Your parent is the scene, therefore your anchor is the scene anchor
            else
            {
                return new XElement("ParentAnchor", HydraExporter.SceneAnchorName);
            }
        }
    }
}
