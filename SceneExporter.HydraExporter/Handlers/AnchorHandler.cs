﻿using PsdExporter;
using System;
using System.Xml.Linq;

namespace SceneExporter.HydraExporter.Handlers
{
    public class AnchorHandler : PsdObjectHandler
    {
        public override Boolean Create(PsdObject psdObject, PsdObject psdObjectAnchor, out XElement[] elements)
        {
            elements = null;

            if (!psdObject.Type.Equals("Anchor")) return false;

            XElement element = new XElement("Anchor");

            //Where this objects position is relative to
            Int32[] rootOffset = psdObjectAnchor?.Offset ?? new[] { 0, 0 };

            element.Add(new XAttribute("ReferenceName", $"{psdObject.Name}"));

            if (psdObject.Parent != null && psdObject.Parent.Type != "Pack")
            {
                element.Add(new XElement("ParentAnchor", $"{psdObject.Parent.Name}-Anchor"));

                XElement offset = new XElement("Offset");
                offset.Add(new XAttribute("X", psdObject.Offset[0] - rootOffset[0]));
                offset.Add(new XAttribute("Y", psdObject.Offset[1] - rootOffset[1]));
                element.Add(offset);
            }
            //Your parent is the scene, therefore your anchor is the scene anchor
            else
            {
                /*String sceneAnchorName = HydraExporter.SceneAnchorName;
                if (psdObject.Name.Equals(sceneAnchorName)) sceneAnchorName = "ScreenContainerTL";
                element.Add(new XElement("ParentAnchor", sceneAnchorName));*/

                XElement offset = new XElement("Position");
                offset.Add(new XAttribute("X", psdObject.Offset[0] - rootOffset[0]));
                offset.Add(new XAttribute("Y", psdObject.Offset[1] - rootOffset[1]));
                element.Add(offset);
            }

            elements = new[] { element };

            return true;
        }

        /// <summary>Will be invoked for every child on a <see cref="PsdObject"/> that was just handled.
        /// Used if the Handler reads all children in one go, and will then ignore the parsing of each individual child there after.
        /// Eg. AnimSprite creates Frame Data from children immediately, does not need to process each child after that if it is a Sprite.</summary>
        /// <param name="psdObject"></param>
        /// <returns></returns>
        public override Boolean ChildAlreadyParsed(PsdObject psdObject)
        {
            return false;
        }
    }
}
