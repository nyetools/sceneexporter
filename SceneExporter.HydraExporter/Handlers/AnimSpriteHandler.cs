﻿using PsdExporter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace SceneExporter.HydraExporter.Handlers
{
    public class AnimSpriteHandler : PsdObjectHandler
    {
        private PsdObject _Anchor;

        public override Boolean Create(PsdObject psdObject, PsdObject psdObjectAnchor, out XElement[] elements)
        {
            _Anchor = psdObjectAnchor;
            elements = null;

            if (!psdObject.Type.Equals("Anim")) return false;

            Dictionary<string, string> data = psdObject.GetData(true);
            int[] anchorOffset = { 0, 0 };
            if (data.TryGetValue("Origin", out string originData))
            {
                string[] split = originData.Split(',');
                anchorOffset = new[] { int.Parse(split[0]), int.Parse(split[1]) };
            }

            PsdObject anchorPsdObject = new PsdObject
            {
                Name = HydraExporter.SceneAnchorName,
                Type = "Anchor",
                Offset = anchorOffset
            };
            new AnchorHandler().Create(anchorPsdObject, null, out XElement[] sceneAnchorElements);

            foreach (XElement sceneAnchorElement in sceneAnchorElements)
            {
                HydraExporter.MainElement.Add(sceneAnchorElement);
            }

            XElement element = new XElement("AnimSprite");

            //Where this objects position is relative to
            Int32[] rootOffset = new[] { 0, 0 };

            element.Add(new XAttribute("ReferenceName", $"{psdObject.Name}"));

            element.Add(GetAnchor(psdObject, psdObjectAnchor));

            element.Add(new XElement("Visible", psdObject.Visible));


            element.Add(new XElement("Z", HydraExporter.GetZ()));

            Dictionary<string, string> allData = psdObject.Parent.GetData(true);

            int[] alignmentMove = new int[] { 0, 0 };

            if (allData.TryGetValue("VAlign", out string vAlignData))
            {
                if ("Left" == vAlignData) vAlignData = "LEFT";
                else if ("Right" == vAlignData)
                {
                    alignmentMove[0] = psdObject.Size[0];
                    vAlignData = "RIGHT";
                }
                else
                {
                    alignmentMove[0] = psdObject.Size[0] / 2;
                    vAlignData = "CENTER";
                }

                element.Add(new XElement("VerticalAlignment", vAlignData));
            }

            if (allData.TryGetValue("HAlign", out string hAlignData))
            {
                if ("Top" == hAlignData) hAlignData = "TOP";
                else if ("Bottom" == hAlignData)
                {
                    alignmentMove[1] = psdObject.Size[1];
                    hAlignData = "BOTTOM";
                }
                else
                {
                    alignmentMove[1] = psdObject.Size[1] / 2;
                    hAlignData = "CENTER";
                }

                element.Add(new XElement("HorizontalAlignment", hAlignData));
            }

            XElement offset = new XElement("Offset");
            offset.Add(new XAttribute("X", psdObject.Offset[0] - rootOffset[0] + alignmentMove[0]));
            offset.Add(new XAttribute("Y", psdObject.Offset[1] - rootOffset[1] + alignmentMove[1]));
            element.Add(offset);

            XElement scaleOrigin = new XElement("Scale");
            scaleOrigin.Add(new XAttribute("X", 1));
            scaleOrigin.Add(new XAttribute("Y", 1));
            element.Add(scaleOrigin);

            XElement isPlaying = new XElement("IsPlaying", false);
            element.Add(isPlaying);

            XElement fpsElement = new XElement("FPS", 30);
            element.Add(fpsElement);

            string endBehaviour = "LOOP_AT_END";
            if (allData.TryGetValue("EndBehaviour", out string endBehaviourValue))
            {
                if ("Loop" == endBehaviourValue) endBehaviour = "LOOP_AT_END";
                else if ("End" == endBehaviourValue) endBehaviour = "STOP_AT_END";
                else if ("Hide" == endBehaviourValue) endBehaviour = "STOP_AND_HIDE";
            }
            XElement endBehaviourElement = new XElement("EndBehaviour", endBehaviour);
            element.Add(endBehaviourElement);

            List<PsdObject> spriteChildren = psdObject.Children.Where(s => "Sprite" == s.Type).ToList();
            PsdObject[] children = spriteChildren.OrderBy(s =>
            {
                String pngGone = s.Name.TrimEnd(".png".ToCharArray());
                String[] splitDash = pngGone.Split('-');
                if (!splitDash.Any()) return 0;

                return Int32.TryParse(splitDash.Last(), out Int32 result) ? result : 0;
            }).ToArray();

            #region FrameList

            XElement frameListElement = new XElement("FrameList");

            for (Int32 i = 0; i < children.Length; i++)
            {
                PsdObject psdObjectChild = children[i];
                frameListElement.Add(new XElement("Texture", psdObjectChild.TexturePath));
            }

            element.Add(frameListElement);

            #endregion

            if (false)
            {
                #region CustomObject FrameData

                XElement offsetAnimData = new XElement("CustomObject");
                offsetAnimData.Add(new XAttribute("Type", "OffsetAnimData"));
                offsetAnimData.Add(new XAttribute("ReferenceName", $"{psdObject.Name}-OffsetAnimData"));

                for (Int32 i = 0; i < children.Length; i++)
                {
                    PsdObject psdObjectChild = children[i];
                    XElement offsetFrameData = new XElement($"FrameData-{i}");
                    offsetFrameData.Add(
                        new XAttribute("Offset-X", psdObjectChild.Offset[0] - (_Anchor?.Offset[0] ?? 0)));
                    offsetFrameData.Add(
                        new XAttribute("Offset-Y", psdObjectChild.Offset[1] - (_Anchor?.Offset[1] ?? 0)));
                    offsetAnimData.Add(offsetFrameData);
                }

                elements = new[] { element, offsetAnimData };

                #endregion
            }
            else
            {
                elements = new[] { element };
            }


            return true;
        }


        /// <summary>Will be invoked for every child on a <see cref="PsdObject"/> that was just handled.
        /// Used if the Handler reads all children in one go, and will then ignore the parsing of each individual child there after.
        /// Eg. AnimSprite creates Frame Data from children immediately, does not need to process each child after that if it is a Sprite.</summary>
        /// <param name="psdObject"></param>
        /// <returns></returns>
        public override Boolean ChildAlreadyParsed(PsdObject psdObject)
        {
            return true; // "Anim".Equals(psdObject.Parent?.Type) && "Sprite".Equals(psdObject.Type);
        }
    }
}

