﻿using PsdExporter;
using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace SceneExporter.HydraExporter.Handlers
{
    public class SpriteHandler : PsdObjectHandler
    {
        public override Boolean Create(PsdObject psdObject, PsdObject psdObjectAnchor, out XElement[] elements)
        {
            elements = null;

            if (!psdObject.Type.Equals("Sprite")) return false;
            if ("Anim".Equals(psdObject.Parent?.Type)) return false;

            XElement element = new XElement("Sprite");

            //Where this objects position is relative to
            Int32[] rootOffset = psdObjectAnchor?.Offset ?? new[] { 0, 0 };

            element.Add(new XAttribute("ReferenceName", $"{psdObject.Name}"));

            element.Add(GetAnchor(psdObject, psdObjectAnchor));

            element.Add(new XElement("Visible", psdObject.Visible));

            element.Add(new XElement("Z", HydraExporter.GetZ()));

            int[] alignmentMove = new int[] { 0, 0 };

            if (psdObject.Parent?.Group != null)
            {
                Dictionary<string, string> allData = psdObject.Parent.GetData(true);

                if (allData.TryGetValue("$VAlign", out string vAlignData))
                {
                    if ("Left" == vAlignData) vAlignData = "LEFT";
                    else if ("Right" == vAlignData)
                    {
                        alignmentMove[0] = psdObject.Size[0];
                        vAlignData = "RIGHT";
                    }
                    else
                    {
                        alignmentMove[0] = psdObject.Size[0] / 2;
                        vAlignData = "CENTER";
                    }
                    element.Add(new XElement("VerticalAlignment", vAlignData));
                }

                if (allData.TryGetValue("$HAlign", out string hAlignData))
                {
                    if ("Top" == hAlignData) hAlignData = "TOP";
                    else if ("Bottom" == hAlignData)
                    {
                        alignmentMove[1] = psdObject.Size[1];
                        hAlignData = "BOTTOM";
                    }
                    else
                    {
                        alignmentMove[1] = psdObject.Size[1] / 2;
                        hAlignData = "CENTER";
                    }
                    element.Add(new XElement("HorizontalAlignment", hAlignData));
                }
            }

            XElement offset = new XElement("Offset");
            offset.Add(new XAttribute("X", psdObject.Offset[0] - rootOffset[0] + alignmentMove[0]));
            offset.Add(new XAttribute("Y", psdObject.Offset[1] - rootOffset[1] + alignmentMove[1]));
            element.Add(offset);

            XElement size = new XElement("Size");
            size.Add(new XAttribute("X", psdObject.Size[0]));
            size.Add(new XAttribute("Y", psdObject.Size[1]));
            element.Add(size);

            XElement scaleOrigin = new XElement("Scale");
            scaleOrigin.Add(new XAttribute("X", 1));
            scaleOrigin.Add(new XAttribute("Y", 1));
            element.Add(scaleOrigin);

            if (psdObject.TexturePath != null)
            {
                XElement texture = new XElement("TextureName", psdObject.TexturePath);
                element.Add(texture);
            }

            elements = new[] { element };

            return true;
        }

        /// <summary>Will be invoked for every child on a <see cref="PsdObject"/> that was just handled.
        /// Used if the Handler reads all children in one go, and will then ignore the parsing of each individual child there after.
        /// Eg. AnimSprite creates Frame Data from children immediately, does not need to process each child after that if it is a Sprite.</summary>
        /// <param name="psdObject"></param>
        /// <returns></returns>
        public override Boolean ChildAlreadyParsed(PsdObject psdObject)
        {
            return false;
        }
    }
}
