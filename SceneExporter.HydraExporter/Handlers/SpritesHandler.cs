﻿using PsdExporter;
using System.Collections.Generic;
using System.Xml.Linq;

namespace SceneExporter.HydraExporter.Handlers
{
    public class SpritesHandler : PsdObjectHandler
    {
        #region Overrides of PsdObjectHandler

        public override bool Create(PsdObject psdObject, PsdObject psdObjectAnchor, out XElement[] elements)
        {
            elements = null;
            if (psdObject.Group != "Sprites") return false;

            Dictionary<string, string> data = psdObject.GetData(true);
            int[] anchorOffset = { 0, 0 };
            if (data.TryGetValue("Origin", out string originData))
            {
                string[] split = originData.Split(',');
                anchorOffset = new[] { int.Parse(split[0]), int.Parse(split[1]) };
            }

            PsdObject anchorPsdObject = new PsdObject
            {
                Name = HydraExporter.SceneAnchorName,
                Type = "Anchor",
                Offset = anchorOffset
            };
            new AnchorHandler().Create(anchorPsdObject, null, out XElement[] sceneAnchorElements);

            foreach (XElement sceneAnchorElement in sceneAnchorElements)
            {
                HydraExporter.MainElement.Add(sceneAnchorElement);
            }

            // ---------------------------

            List<XElement> spriteElements = new List<XElement>();
            SpriteHandler spriteHandler = new SpriteHandler();

            foreach (PsdObject psdObjectChild in psdObject.Children)
            {
                if (spriteHandler.Create(psdObjectChild, anchorPsdObject, out XElement[] childElements)
                    && childElements != null)
                {
                    spriteElements.AddRange(childElements);
                    HydraExporter.IncrementZ();
                }
            }

            elements = spriteElements.ToArray();
            return true;
        }

        public override bool ChildAlreadyParsed(PsdObject psdObject)
        {
            return true;
        }

        #endregion
    }
}
