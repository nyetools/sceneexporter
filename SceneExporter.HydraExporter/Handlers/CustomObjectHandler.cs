﻿using PsdExporter;
using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace SceneExporter.HydraExporter.Handlers
{
    public class CustomObjectHandler : PsdObjectHandler
    {
        public override Boolean Create(PsdObject psdObject, PsdObject psdObjectAnchor, out XElement[] elements)
        {
            elements = null;

            if (!psdObject.Type.Equals("Group")) return false;

            if (HydraExporter.GetObjectName(psdObject).StartsWith("[$")) return false;

            XElement element = new XElement("CustomObject");

            element.Add(new XAttribute("ObjectType", psdObject.Group));

            element.Add(new XAttribute("ReferenceName", $"{psdObject.Name}"));

            element.Add(new XElement("Visible", psdObject.Visible));

            element.Add(new XElement("Z", HydraExporter.GetZ()));

            Dictionary<string, string> data = psdObject.GetData();
            foreach (KeyValuePair<string, string> dataValue in data)
            {
                if (dataValue.Value == null) continue;

                string[] split = dataValue.Value.Split(',');
                string key = dataValue.Key;
                if (key.StartsWith("$")) key = key.Substring(1, key.Length - 1);

                if (split.Length == 1)
                {
                    XElement dataElement = new XElement(key, split[0]);
                    element.Add(dataElement);
                }
                else if (split.Length == 2)
                {
                    XElement dataElement = new XElement(key);
                    dataElement.Add(new XAttribute("X", split[0]));
                    dataElement.Add(new XAttribute("Y", split[1]));
                    element.Add(dataElement);
                }
                else if (split.Length == 3)
                {
                    XElement dataElement = new XElement(key);
                    dataElement.Add(new XAttribute("X", split[0]));
                    dataElement.Add(new XAttribute("Y", split[1]));
                    dataElement.Add(new XAttribute("Z", split[2]));
                    element.Add(dataElement);
                }
                else if (split.Length == 4)
                {
                    XElement dataElement = new XElement(key);
                    dataElement.Add(new XAttribute("X", split[0]));
                    dataElement.Add(new XAttribute("Y", split[1]));
                    dataElement.Add(new XAttribute("Width", split[2]));
                    dataElement.Add(new XAttribute("Height", split[3]));
                    element.Add(dataElement);
                }
            }

            elements = new[] { element };

            return true;
        }

        /// <summary>Will be invoked for every child on a <see cref="PsdObject"/> that was just handled.
        /// Used if the Handler reads all children in one go, and will then ignore the parsing of each individual child there after.
        /// Eg. AnimSprite creates Frame Data from children immediately, does not need to process each child after that if it is a Sprite.</summary>
        /// <param name="psdObject"></param>
        /// <returns></returns>
        public override Boolean ChildAlreadyParsed(PsdObject psdObject)
        {
            return false;
        }
    }
}
