﻿using SceneExtractor;
using System;

namespace SceneExporter
{
    class Program
    {
        public static void Main(params String[] args)
        {
            PsdExtractor extractor = new PsdExtractor(args[0]);

            extractor.OnWriteLine += Console.WriteLine;
            extractor.OnError += s =>
            {
                Console.ReadKey(true);
            };
            extractor.OnVersionIdentified += s => { Console.Title = s; };

            extractor.Extract();
        }
    }

}


