﻿using PsdExporter;
using System;
using System.Linq;
using System.Xml.Linq;

namespace SceneExporter.KrakenExporter.Handlers
{
    public class AnimSpriteHandler : PsdObjectHandler
    {
        private PsdObject _Anchor;

        public override Boolean Create(PsdObject psdObject, PsdObject psdObjectAnchor, out XElement element)
        {
            _Anchor = psdObjectAnchor;
            element = null;

            if (!psdObject.Type.Equals("Anim")) return false;

            element = new XElement("AnimSprite");

            //Where this objects position is relative to
            Int32[] rootOffset = new[] { 0, 0 };

            element.Add(new XAttribute("ReferenceName", $"{psdObject.Name}"));

            element.Add(GetAnchor(psdObject, psdObjectAnchor));

            element.Add(new XElement("Visible", psdObject.Visible));

            element.Add(new XElement("Z", KrakenExporter.GetZ()));

            XElement offset = new XElement("Offset");
            offset.Add(new XAttribute("X", psdObject.Offset[0] - rootOffset[0]));
            offset.Add(new XAttribute("Y", psdObject.Offset[1] - rootOffset[1]));
            element.Add(offset);

            XElement scaleOrigin = new XElement("Scale");
            scaleOrigin.Add(new XAttribute("X", 1));
            scaleOrigin.Add(new XAttribute("Y", 1));
            element.Add(scaleOrigin);

            PsdObject[] children = psdObject.Children.OrderBy(s =>
            {
                String pngGone = s.Name.TrimEnd(".png".ToCharArray());
                String[] splitDash = pngGone.Split('-');
                if (!splitDash.Any()) return 0;

                return Int32.TryParse(splitDash.Last(), out Int32 result) ? result : 0;
            }).ToArray();

            #region FrameList

            XElement frameListElement = new XElement("FrameList");

            for (Int32 i = 0; i < children.Length; i++)
            {
                PsdObject psdObjectChild = children[i];
                frameListElement.Add(new XElement("Texture", psdObjectChild.TexturePath));
            }

            element.Add(frameListElement);

            #endregion

            return true;
        }

        #region Overrides of PsdObjectHandler

        public override XElement[] CreateExtraElements(PsdObject psdObject, PsdObject psdObjectAnchor)
        {
            PsdObject[] children = psdObject.Children.OrderBy(s =>
            {
                String pngGone = s.Name.TrimEnd(".png".ToCharArray());
                String[] splitDash = pngGone.Split('-');
                if (!splitDash.Any()) return 0;

                return Int32.TryParse(splitDash.Last(), out Int32 result) ? result : 0;
            }).ToArray();

            XElement offsetAnimData = new XElement("CustomObject");
            offsetAnimData.Add(new XAttribute("Type", "OffsetAnimData"));
            offsetAnimData.Add(new XAttribute("ReferenceName", $"{psdObject.Name}-OffsetAnimData"));

            for (Int32 i = 0; i < children.Length; i++)
            {
                PsdObject psdObjectChild = children[i];
                XElement offsetFrameData = new XElement($"FrameData-{i}");
                offsetFrameData.Add(new XAttribute("Offset-X", psdObjectChild.Offset[0] - (_Anchor?.Offset[0] ?? 0)));
                offsetFrameData.Add(new XAttribute("Offset-Y", psdObjectChild.Offset[1] - (_Anchor?.Offset[1] ?? 0)));
                offsetAnimData.Add(offsetFrameData);
            }

            return new[] { offsetAnimData };
        }

        #endregion

        /// <summary>Will be invoked for every child on a <see cref="PsdObject"/> that was just handled.
        /// Used if the Handler reads all children in one go, and will then ignore the parsing of each individual child there after.
        /// Eg. AnimSprite creates Frame Data from children immediately, does not need to process each child after that if it is a Sprite.</summary>
        /// <param name="psdObject"></param>
        /// <returns></returns>
        public override Boolean ChildAlreadyParsed(PsdObject psdObject)
        {
            return true;// "Anim".Equals(psdObject.Parent?.Type) && "Sprite".Equals(psdObject.Type);
        }
    }
}
