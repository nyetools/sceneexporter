﻿using PsdExporter;
using System;
using System.Xml.Linq;

namespace SceneExporter.KrakenExporter.Handlers
{
    public class SpriteHandler : PsdObjectHandler
    {
        public override Boolean Create(PsdObject psdObject, PsdObject psdObjectAnchor, out XElement element)
        {
            element = null;

            if (!psdObject.Type.Equals("Sprite")) return false;
            if ("Anim".Equals(psdObject.Parent?.Type)) return false;

            element = new XElement("Sprite");

            //Where this objects position is relative to
            Int32[] rootOffset = psdObjectAnchor?.Offset ?? new[] { 0, 0 };

            element.Add(new XAttribute("ReferenceName", $"{psdObject.Name}"));

            element.Add(GetAnchor(psdObject, psdObjectAnchor));

            element.Add(new XElement("Visible", psdObject.Visible));

            element.Add(new XElement("Z", KrakenExporter.GetZ()));

            XElement offset = new XElement("Offset");
            offset.Add(new XAttribute("X", psdObject.Offset[0] - rootOffset[0]));
            offset.Add(new XAttribute("Y", psdObject.Offset[1] - rootOffset[1]));
            element.Add(offset);

            XElement size = new XElement("Size");
            size.Add(new XAttribute("X", psdObject.Size[0]));
            size.Add(new XAttribute("Y", psdObject.Size[1]));
            element.Add(size);

            XElement scaleOrigin = new XElement("Scale");
            scaleOrigin.Add(new XAttribute("X", 1));
            scaleOrigin.Add(new XAttribute("Y", 1));
            element.Add(scaleOrigin);

            if (psdObject.TexturePath != null)
            {
                XElement texture = new XElement("Texture", psdObject.TexturePath);
                element.Add(texture);
            }

            return true;
        }

        /// <summary>Will be invoked for every child on a <see cref="PsdObject"/> that was just handled.
        /// Used if the Handler reads all children in one go, and will then ignore the parsing of each individual child there after.
        /// Eg. AnimSprite creates Frame Data from children immediately, does not need to process each child after that if it is a Sprite.</summary>
        /// <param name="psdObject"></param>
        /// <returns></returns>
        public override Boolean ChildAlreadyParsed(PsdObject psdObject)
        {
            return false;
        }
    }
}
