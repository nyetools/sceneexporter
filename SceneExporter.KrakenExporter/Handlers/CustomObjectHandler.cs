﻿using PsdExporter;
using System;
using System.Xml.Linq;

namespace SceneExporter.KrakenExporter.Handlers
{
    public class CustomObjectHandler : PsdObjectHandler
    {
        public override Boolean Create(PsdObject psdObject, PsdObject psdObjectAnchor, out XElement element)
        {
            element = null;

            if (!psdObject.Type.Equals("Group")) return false;

            element = new XElement("CustomObject");

            element.Add(new XAttribute("Type", psdObject.Group));

            element.Add(new XAttribute("ReferenceName", $"{psdObject.Name}"));

            element.Add(new XElement("Visible", psdObject.Visible));

            element.Add(new XElement("Z", KrakenExporter.GetZ()));

            return true;
        }

        /// <summary>Will be invoked for every child on a <see cref="PsdObject"/> that was just handled.
        /// Used if the Handler reads all children in one go, and will then ignore the parsing of each individual child there after.
        /// Eg. AnimSprite creates Frame Data from children immediately, does not need to process each child after that if it is a Sprite.</summary>
        /// <param name="psdObject"></param>
        /// <returns></returns>
        public override Boolean ChildAlreadyParsed(PsdObject psdObject)
        {
            return false;
        }
    }
}
