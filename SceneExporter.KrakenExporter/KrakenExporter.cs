﻿using PsdExporter;
using SceneExporter.KrakenExporter.Handlers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace SceneExporter.KrakenExporter
{
    [ExportType(".xml", "Kraken_Exporter")]
    public class KrakenExporter : Exporter
    {
        private static Int32 _Z;

        private static String _ZBase;

        public static String GetZ()
        {
            return $"{_ZBase}_{_Z}";
        }

        private ExportSettings _Settings;

        private XElement _Element;

        private List<PsdObjectHandler> _ObjectHandlers = new List<PsdObjectHandler>();

        public static String SceneName { get; private set; }

        public static String SceneAnchorName => $"ScreenContainerTL";

        public override void Start(String sceneName, ExportSettings settings)
        {
            SceneName = sceneName;
            _ZBase = sceneName;
            _Settings = settings;
            _Z = 0;

            _Settings.InferPackWrapAround = true;

            _ObjectHandlers = new List<PsdObjectHandler>
            {
                new SpriteHandler(),
                new AnchorHandler(),
                new AnimSpriteHandler(),
                new CustomObjectHandler(),
            };

            _Element = new XElement("Scene");
            _Element.Add(new XAttribute("ReferenceName", sceneName));
            _Element.Add(new XElement("Visible", true));
        }

        private List<PsdObject> GetAll(PsdObject psdObject, Predicate<PsdObject> pattern, Boolean removeOnMatchFromParent = false)
        {
            List<PsdObject> psdObjects = new List<PsdObject>();
            Search(psdObject, psdObjects, pattern, removeOnMatchFromParent);
            return psdObjects;
        }

        private void Search(PsdObject psdObject, List<PsdObject> psdObjects, Predicate<PsdObject> pattern, Boolean removeOnMatchFromParent = false)
        {
            if (pattern?.Invoke(psdObject) ?? false)
            {
                psdObjects.Add(psdObject);
                if (removeOnMatchFromParent)
                {
                    psdObject.Parent?.Children.Remove(psdObject);
                }
            }

            for (Int32 i = psdObject.Children.Count - 1; i >= 0; i--)
            {
                PsdObject psdObjectChild = psdObject.Children[i];
                Search(psdObjectChild, psdObjects, pattern, removeOnMatchFromParent);
            }
        }

        public override void Process(PsdObject[] packs)
        {
            //Dont output anything for packs
            foreach (PsdObject packObject in packs)
            {
                //All objects in packs need to be added

                for (Int32 i = 0; i < packObject.Children.Count; i++)
                {
                    PsdObject psdObject = packObject.Children[i];
                    //_ZBase = groupObject.Name;

                    _Element.Add(GetElement(psdObject, out XElement[] extraElements, null));

                    if (extraElements != null)
                    {
                        foreach (XElement extraElement in extraElements)
                        {
                            _Element.Add(extraElement);
                        }
                    }

                    _Z++;
                }

                _Z++;
            }
        }

        private XElement GetElement(PsdObject psdObject, out XElement[] extraElements, PsdObject anchor = null)
        {
            if (psdObject.Type == "Group")
            {
                //Find the first anchor inside the group, this will be the anchor used for the group, rest will be ignored
                List<PsdObject> anchors = GetAll(psdObject, o => "Anchor".Equals(o.Type), true).Where(s => s != psdObject).ToList();

                if (anchors.Any(s => s.Name.StartsWith(psdObject.Name)))
                {
                    anchor = anchors.First(s => s.Name.StartsWith(psdObject.Name));
                }
                else
                {
                    anchor = new PsdObject
                    {
                        Type = "Anchor",
                        Name = $"{psdObject.Name}-Anchor",
                        Offset = new[] { psdObject.Offset[0], psdObject.Offset[1] },
                    };

                    psdObject.Offset = new[] { 0, 0 };
                    Dictionary<string, string> anchorData = psdObject.GetData(true);
                    if (anchorData.TryGetValue("$Origin", out String anchorOrigin))
                    {
                        string[] split = anchorOrigin.Split(',');
                        psdObject.Offset = new[] { int.Parse(split[0]), int.Parse(split[1]) };
                    }
                }

                //Remove the group from the hierarchy, as Anchors should be outside the scope of the object,
                //but is more confusing to make them like that in Photoshop
                anchor.Parent = psdObject.Parent;

                _Element.Add(GetElement(anchor, out _));
            }


            PsdObjectHandler handler = null;
            XElement element = null;
            extraElements = null;

            foreach (PsdObjectHandler psdObjectHandler in _ObjectHandlers)
            {
                if (!psdObjectHandler.Create(psdObject, anchor, out element)) continue;
                handler = psdObjectHandler;
                break;
            }

            if (element == null)
            {
                Console.WriteLine($"No handler for {psdObject.Type}");
                return null;
            }


            List<PsdObject> psdObjectChildren = psdObject.Children;

            if (psdObjectChildren.Any())
            {
                //psdObjectChildren.Reverse();
                foreach (PsdObject psdObjectChild in psdObjectChildren)
                {
                    if (handler?.ChildAlreadyParsed(psdObjectChild) ?? false) continue;

                    XElement childElement = GetElement(psdObjectChild, out XElement[] extraChildElements, anchor);

                    if (childElement == null) continue;

                    _Z++;
                    element.Add(childElement);

                    if (extraChildElements != null)
                    {
                        foreach (XElement extraChildElement in extraChildElements)
                        {
                            element.Add(extraChildElement);

                            _Z++;
                        }
                    }
                }
            }

            extraElements = handler?.CreateExtraElements(psdObject, anchor);

            return element;
        }

        public override String End()
        {
            return _Element.ToString();
        }

        public override String GetOutputFilePath()
        {
            return Path.Combine($"X{_Settings.PackName}", $"{_Settings.OutputFileName}.xml");
        }
    }
}
