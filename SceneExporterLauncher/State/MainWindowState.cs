﻿using Newtonsoft.Json;
using PsdExporter;
using SceneExtractor;
using SceneExtractor.Plugins;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SceneExporterLauncher.State
{
    public class MainWindowState
    {
        private MainWindow _MainWindow;

        private String _OutputFolderDefaultText;
        private TextBox _OutputTextBox;

        private Dictionary<String, Type> _ExportPlugins;

        private String _psdPath;
        private String _psdFile;
        private String _psdName;

        private String _ExporterName;

        private String _OutputFolder;
        private String _OutputFileName;
        private String _OutputSceneName;
        private String _PackName;

        public MainWindowState(MainWindow mainWindow)
        {
            _MainWindow = mainWindow;
            Version version = Assembly.GetAssembly(typeof(MainWindowState)).GetName().Version;
            _MainWindow.Title = $"SceneExporter - {version}";

            _OutputTextBox = _MainWindow.OutputFolderLabel;
            _OutputFolderDefaultText = _OutputTextBox.Text;

            List<Type> plugins = new List<Type>();
            Type[] additionalPlugins = PluginLoader.Load();

            plugins.AddRange(additionalPlugins);

            _ExportPlugins = new Dictionary<String, Type>();
            foreach (Type plugin in plugins)
            {
                String name = $"{plugin.GetCustomAttribute<ExportTypeAttribute>().HumanName} | {FileVersionInfo.GetVersionInfo(plugin.Assembly.Location).FileVersion}";
                if (_ExportPlugins.ContainsKey(name)) continue;
                _ExportPlugins.Add(name, plugin);

                _MainWindow.ExporterComboBox.Items.Add(name);
            }
        }

        private void UpdateState()
        {
            Boolean psdLoaded = !String.IsNullOrEmpty(_psdPath);
            Boolean outputSelected = !_OutputFolderDefaultText.Equals(_OutputTextBox.Text);
            Boolean exporterSelected = !String.IsNullOrEmpty(_ExporterName);

            _MainWindow.ImportButton.Opacity = !psdLoaded ? 1 : 0.8;
            _MainWindow.ImportButton.Content = !psdLoaded ? "Import Psd" : _psdFile;

            _MainWindow.ExportButton.Opacity = psdLoaded && outputSelected && exporterSelected ? 1 : 0.8;
            _MainWindow.ExportButton.IsEnabled = psdLoaded && outputSelected && exporterSelected;
            _MainWindow.ExportButton.Content = "Export";

            _PackName = _MainWindow.PackNameTextBox.Text;
        }

        private void Reset()
        {
            _psdPath = _psdFile = _psdName = null;
            UpdateState();
        }

        public Boolean IsPsdLoaded() => !String.IsNullOrEmpty(_psdPath);

        public void PsdSelected(String psdPath)
        {
            _psdPath = psdPath;
            _psdFile = _psdPath.Split('\\').Last();
            _psdName = _psdFile.Split('.')[0];

            UpdateState();

            if (!String.IsNullOrEmpty(_psdName))
            {
                _MainWindow.FileNameTextBox.Text = _psdName;
            }
        }

        public void PsdDeselected()
        {
            _psdPath = _psdFile = _psdName = null;
            UpdateState();
        }

        public void OutputFolderSelected(String folderBrowserSelectedPath)
        {
            _OutputTextBox.Text = _OutputFolder = folderBrowserSelectedPath;

            UpdateState();
        }

        public void SceneNameChanged()
        {
            _OutputSceneName = _MainWindow.SceneNameTextBox.Text;
            UpdateState();
        }

        public void ExporterChanged()
        {
            _ExporterName = _MainWindow.ExporterComboBox.SelectedItem as String;
            UpdateState();
        }

        public void ExportPsd()
        {
            UpdateState();

            _OutputFileName = _MainWindow.FileNameTextBox.Text;
            _OutputSceneName = _MainWindow.SceneNameTextBox.Text;

            Assembly assembly = Assembly.GetAssembly(typeof(MainWindow));
            String directory = Path.GetDirectoryName(assembly.Location);
            DirectoryInfo d = new DirectoryInfo(directory);

            StartUpArguments startup = new StartUpArguments()
            {
                SceneName = _OutputSceneName,
                FileName = _OutputFileName,
                PSDPath = _psdPath,
                OutputDirectory = _OutputFolder,
                ExporterName = _ExporterName.Split('|')[0].Trim(),
                PackName = _PackName,

                PauseAtEnd = false,
                AutoOpen = false,
                IgnoreInvisibleLayers = bool.Parse(_MainWindow.IgnoreInvisibleCheckBox.IsChecked.ToString().ToLowerInvariant()),

                PluginDirectory = $"{new DirectoryInfo(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location))}\\Plugins\\",

                ReverseChildOrder = bool.Parse(_MainWindow.TopToBottom.IsChecked.ToString().ToLowerInvariant()),

                ParentPrefixOnNames = bool.Parse(_MainWindow.ParentPrefix.IsChecked.ToString().ToLowerInvariant()),

                CustomArguments = new StartUpArguments.CustomData[]
                {
                    new StartUpArguments.CustomData("SceneAnchor",$"{_OutputSceneName}Anchor"),
                }
            };

            string jsonArgs = JsonConvert.SerializeObject(startup, Formatting.Indented);

            try
            {
                using (StreamWriter stream = File.CreateText("args.json"))
                {
                    stream.Write(jsonArgs);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            RunExporter();
        }

        private async void RunExporter()
        {
            _MainWindow.ExportButton.Content = "Exporting..";
            _MainWindow.ExportButton.Opacity = 0.8f;
            _MainWindow.ExportButton.IsEnabled = false;

            Task running = Task.Run(() =>
            {
                PsdExtractor extractor = new PsdExtractor($"args.json");

                extractor.OnWriteLine += s => { };

                //TODO This needs to not lockup the execution of code, use task await?
                extractor.Extract();
            });

            await running;

            Reset();
        }
    }
}
