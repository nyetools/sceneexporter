﻿using Microsoft.Win32;
using SceneExporterLauncher.State;
using System;
using System.Windows;

namespace SceneExporterLauncher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowState _State;

        public MainWindow()
        {
            InitializeComponent();
            _State = new MainWindowState(this);

            TopToBottom.IsChecked = true;
        }

        private void Button_Click(Object sender, RoutedEventArgs e)
        {
            if (!_State.IsPsdLoaded())
            {
                OpenFileDialog dialog = new OpenFileDialog
                {
                    Filter = "Psd files (*.psd)|*.psd"
                };

                if (dialog.ShowDialog() == true)
                {
                    _State.PsdSelected(dialog.FileName);
                }
            }
            else
            {
                _State.PsdDeselected();
            }
        }

        private void OutputFolderButton_Click(Object sender, RoutedEventArgs e)
        {
            Ookii.Dialogs.Wpf.VistaFolderBrowserDialog folderBrowser = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();

            if (folderBrowser.ShowDialog(this).GetValueOrDefault())
            {
                _State.OutputFolderSelected(folderBrowser.SelectedPath);
            }
        }

        private void SceneNameTextBox_TextChanged(Object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            _State?.SceneNameChanged();
        }

        private void ExporterComboBox_SelectionChanged(Object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            _State.ExporterChanged();
        }

        private void ExportButton_Click(Object sender, RoutedEventArgs e)
        {
            _State.ExportPsd();
        }

        private void TopToBottom_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void ParentPrefix_Checked(object sender, RoutedEventArgs e)
        {

        }
    }
}