﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PsdExporter
{
    public class PsdObject
    {
        /// <summary>The Pack folder this object should be part of</summary>
        public String Pack { get; set; }

        /// <summary>The name of this particular object</summary>
        public String Name { get; set; }

        /// <summary>Special type data, eg Sprite, AnimSprite</summary>
        public String Type { get; set; }

        /// <summary>Contains extra information used for <see cref="Type"/></summary>
        public String Group { get; set; }

        /// <summary>Where the .png has been exported to in the file structure</summary>
        public String TexturePath { get; set; }

        /// <summary>Postional value away from the Root</summary>
        public Int32[] Offset { get; set; } = new Int32[2];

        /// <summary>Texture size</summary>
        public Int32[] Size { get; set; } = new Int32[2];

        /// <summary>Whether this object should be visible or not</summary>
        public Boolean Visible { get; set; }

        /// <summary>Object that owns this object</summary>
        public PsdObject Parent { get; set; }

        /// <summary>Object's this object owns</summary>
        public List<PsdObject> Children { get; } = new List<PsdObject>();

        /// <summary>All Data that is attached to this PSDObject through [TAG:INFO] format</summary>
        public Dictionary<String, String> Data { get; set; } = new Dictionary<string, string>();

        public override String ToString()
        {
            String s = "";
            Boolean t = !String.IsNullOrEmpty(Type);
            Boolean p = !String.IsNullOrEmpty(Pack);
            Boolean g = !String.IsNullOrEmpty(Group);
            Boolean n = !String.IsNullOrEmpty(Name);

            if (p) s += $"{Pack}";
            if (g) s += $"{(p ? "." : "")}{Group}";
            if (t) s += $"{(g ? "." : "")}{Type}";
            if (n) s += $"{(p ? "." : "")}{Name}";

            return s;
        }

        /// <summary>Accumulates all the Data this object holds</summary>
        /// <param name="includeChildrenData">Will include single layer child data as well</param>
        /// <param name="exclude"></param>
        /// <returns></returns>
        public Dictionary<String, String> GetData(Boolean includeChildrenData = true, params String[] exclude)
        {
            Dictionary<String, String> data = Data.ToDictionary(k => k.Key, v => v.Value);

            if (exclude != null) foreach (string s in exclude) if (data.ContainsKey(s)) data.Remove(s);

            if (includeChildrenData)
            {
                foreach (PsdObject psdObject in Children)
                {
                    Dictionary<string, string> childData = psdObject.GetData(true);
                    foreach (KeyValuePair<string, string> cd in childData)
                    {
                        if (exclude != null && exclude.Any(s => s.Equals(cd.Key))) continue;
                        if (data.ContainsKey(cd.Key))
                        {
                            continue;
                        }
                        data.Add(cd.Key, cd.Value);
                    }
                }
            }

            return data;
        }
    }
}