﻿using System;
using System.Collections.Generic;

namespace PsdExporter
{
    public class ExportSettings
    {
        public Dictionary<string, string> CustomArguments { get; set; }

        /// <summary>If true, then all the layers will be wrapped in an object called "Pack"</summary>
        public Boolean InferPackWrapAround { get; set; }

        /// <summary>Where the PSD has been read from</summary>
        public String InputDirectory { get; set; }
        public String InputPsdFileName { get; set; }
        public String PsdFilePath { get; set; }

        /// <summary>Where the exported file will be placed</summary>
        public String OutputDirectory { get; set; }
        public String OutputGraphicsDirectory { get; set; }
        public String OutputXMLDirectory { get; set; }
        public String OutputFileName { get; set; }
        public String OutputSceneName { get; set; }
        public String PackName { get; set; }

        public Boolean IgnoreInvisbleLayers { get; set; }

        /// <summary>If true, then the layers will be reversed when parsed.
        /// True = reading them from top layer to bottom, as displayed in the PSD file itself</summary>
        public Boolean ReverseLayers { get; set; }

        /// <summary>If true, then all objects will have their parents name prefixed to them, separated with dashes</summary>
        public Boolean PrefixParentName { get; set; } = true;

        /// <summary>Exporting will trim any excess white space, but keeps the position of the graphic in the PSD the same</summary>
        public bool TrimOutput { get; set; }
    }
}
