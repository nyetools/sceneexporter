﻿using System;

namespace PsdExporter
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ExportTypeAttribute : System.Attribute
    {
        public String FileExtension { get; }

        public String HumanName { get; }

        public ExportTypeAttribute(String fileExtension, String humanName)
        {
            FileExtension = fileExtension;
            HumanName = humanName;
        }
    }
}
