﻿using System;

namespace PsdExporter
{
    public abstract class Exporter
    {
        /// <summary>Called when the parsing process begins</summary>
        /// <param name="sceneName">The name of the scene storing all the objects</param>
        /// <param name="settings"></param>
        public abstract void Start(String sceneName, ExportSettings settings);

        public abstract void Process(PsdObject[] packs);

        public abstract String End();

        /// <summary>Where you want your XML file to be outputted, will get appended onto <see cref="ExportSettings.OutputXMLDirectory"/></summary>
        /// <returns></returns>
        public abstract String GetOutputFilePath();
    }
}
