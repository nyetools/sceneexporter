﻿using System;

namespace PsdExporter
{
    public class StartUpArguments
    {
        public struct CustomData
        {
            public String Key;
            public String Value;

            public CustomData(string key, string value)
            {
                Key = key;
                Value = value;
                Value = value;
            }
        }

        public string SceneName { get; set; }
        public string FileName { get; set; }
        public string PSDPath { get; set; }
        public string OutputDirectory { get; set; }
        public string ExporterName { get; set; }
        public string PackName { get; set; }
        public bool PauseAtEnd { get; set; }
        public bool AutoOpen { get; set; }
        public bool IgnoreInvisibleLayers { get; set; }
        public string PluginDirectory { get; set; }
        public bool ReverseChildOrder { get; set; }
        public bool ParentPrefixOnNames { get; set; }
        public CustomData[] CustomArguments { get; set; }
    }
}
