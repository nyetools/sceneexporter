﻿using PsdExporter;
using SceneExtractor.PsdCreation.Ntreev;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace SceneExtractor
{
    public partial class PsdExtractor
    {
        public Action<String> OnWriteLine { get; set; }


        public Action<String> OnVersionIdentified { get; set; }

        public Action<String> OnError { get; set; }

        private readonly string[] _PathToArgumentJson;

        private ExportSettings _Settings;

        private Exporter _Exporter;

        public PsdExtractor(params String[] pathToArgumentJson)
        {
            _PathToArgumentJson = pathToArgumentJson;
        }

        public void Extract()
        {
            Version version = Assembly.GetAssembly(typeof(PsdExtractor)).GetName().Version;
            OnVersionIdentified?.Invoke($"SceneExporter - {version}");

            Initialise();
            Type exporterTye = LoadPlugin();

            if (exporterTye == null)
            {
                OnWriteLine?.Invoke($"No Exporter included in Plugin directory");
                OnError?.Invoke($"No Exporter included in Plugin directory");
                return;
            }

            OnWriteLine?.Invoke("Export starting...\n");

            _Exporter = (Exporter)Activator.CreateInstance(exporterTye);

            ExportTypeAttribute exportType = _Exporter.GetType().GetCustomAttribute<ExportTypeAttribute>();
            _Exporter.Start(_Settings.OutputSceneName, _Settings);

            NtreevPsdReader psdReader = new NtreevPsdReader();

            PsdObject[] objectStructure = psdReader.Create(_Settings);

            _Exporter.Process(objectStructure);

            Export(exportType.FileExtension, _Exporter.End());
        }

        private void Export(String fileExtension, String content)
        {
            #region ExportToFile

            OnWriteLine?.Invoke("");

            String p = Path.Combine(_Settings.OutputXMLDirectory, _Exporter.GetOutputFilePath());
            p = p.Replace("\\\\", "\\");
            p = p.Replace("\\", "/");

            String exportDirectory = Path.GetDirectoryName(p);
            if (!Directory.Exists(exportDirectory)) Directory.CreateDirectory(exportDirectory);

            if (File.Exists(p)) File.Delete(p);

            File.WriteAllText(p, content);

            #endregion

            #region EndProcess

            try
            {
                if (_StartUpArguments.AutoOpen)
                {
                    String openReader = "notepad.exe";
                    Process.Start(openReader, p);
                }
            }
            catch (Exception e)
            {
                // ignored
            }

            OnWriteLine?.Invoke($"\"{_Settings.OutputSceneName}{fileExtension}\" created at {p}");

            if (_StartUpArguments.PauseAtEnd)
            {
                Console.Write("\nCompleted, press any key");
                Console.Read();
            }

            #endregion
        }
    }
}
