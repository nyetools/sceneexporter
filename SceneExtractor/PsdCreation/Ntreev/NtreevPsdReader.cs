﻿using ImageMagick;
using Ntreev.Library.IO;
using Ntreev.Library.Psd;
using PsdExporter;
using SceneExporter.HydraExporter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SceneExtractor.PsdCreation.Ntreev
{
    public class NtreevPsdReader
    {
        private Dictionary<String, Int32> _GroupCount = new Dictionary<String, Int32>();

        private Dictionary<String, Int32> _ObjectGroupsCreated = new Dictionary<String, Int32>();

        private Dictionary<PsdObject, IPsdLayer> _PsdObjectLayers = new Dictionary<PsdObject, IPsdLayer>();

        private ExportSettings _Settings;

        #region Implementation of IPsdReader

        public PsdObject[] Create(ExportSettings settings)
        {
            _Settings = settings;

            using (PsdDocument document = PsdDocument.Create(_Settings.PsdFilePath))
            {
                List<PsdObject> objectStructure = new List<PsdObject>();

                PsdObject pack = null;
                if (_Settings.InferPackWrapAround)
                {
                    pack = new PsdObject
                    {
                        Pack = _Settings.PackName,
                        Name = $"Pack-{_Settings.OutputSceneName}",
                        Type = "Pack"
                    };
                    objectStructure.Add(pack);
                }

                PsdLayer[] psdLayers = document.Childs.OfType<PsdLayer>().ToArray();
                if (_Settings.ReverseLayers)
                {
                    psdLayers = psdLayers.Reverse().ToArray();
                }

                foreach (PsdLayer psdLayer in psdLayers)
                {
                    PsdObject psdObject = MakeObject(pack, psdLayer);
                    if (psdObject != null)
                    {
                        if (pack != null)
                        {
                            pack.Children.Add(psdObject);
                        }
                        else
                        {
                            objectStructure.Add(psdObject);
                        }
                    }
                }

                foreach (PsdObject psdObject in objectStructure)
                {
                    OutputPsdObject(psdObject, 0);
                    Console.WriteLine("");
                }

                return objectStructure.ToArray();
            }

            return null;
        }
        #endregion

        private PsdObject MakeObject(PsdObject parent, PsdLayer layer)
        {
            if (_Settings.IgnoreInvisbleLayers && !layer.IsVisible) return null;

            PsdObject psdObject = new PsdObject
            {
                Visible = layer.IsVisible,
                Parent = parent
            };

            psdObject.Data = layer.Name.GetAllData();

            if (psdObject.Data.ContainsKey("Ignore") || psdObject.Data.ContainsKey("ignore")) return null;

            if (layer.HasImage)
            {
                psdObject.Offset = new Int32[]
                {
                    layer.Left,
                    layer.Top
                };

                psdObject.Size = new Int32[]
                {
                    layer.Width,
                    layer.Height
                };
            }

            #region LayerDataSetting

            String layerName = layer.Name;

            if (layerName.GetData("", out String groupName) || layerName.GetData("Group", out groupName))
            {
                if (_ObjectGroupsCreated.ContainsKey(groupName)) _ObjectGroupsCreated[groupName]++;
                else _ObjectGroupsCreated.Add(groupName, 0);

                String myNameFromGroup = groupName;
                if (!_GroupCount.ContainsKey(groupName)) _GroupCount.Add(groupName, 0);
                //TODO - detect if there are any possible other types of the same group available, then you could omit the _0 if not.
                myNameFromGroup = $"{myNameFromGroup}_{_GroupCount[groupName]++}";

                psdObject.Name = myNameFromGroup;
                psdObject.Group = groupName;
                psdObject.Type = "Group";
            }

            if (layerName.GetData("Anim", out String animName))
            {
                if (_ObjectGroupsCreated.ContainsKey(animName)) _ObjectGroupsCreated[animName]++;
                else _ObjectGroupsCreated.Add(animName, 0);

                psdObject.Name = animName;
                String parentgroup = parent?.Group;
                psdObject.Group = $"{(parentgroup != null ? $"{parentgroup}/" : "")}{animName}";
                psdObject.Type = "Anim";
            }

            if (layerName.GetData("Anchor", out String anchorData))
            {
                psdObject.Type = "Anchor";
                psdObject.Name = $"{(parent?.Name ?? "Main")}-Anchor";

                try
                {
                    psdObject.Offset = anchorData.Trim().Split(',').Select(Int32.Parse).ToArray();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Anchor is expecting format of 'Anchor:0,0'");
                    return null;
                }
            }

            if (layerName.GetData("Name", out String nameData))
            {
                psdObject.Name = nameData;
            }

            #endregion

            #region LayerDataDefaults

            if (String.IsNullOrEmpty(psdObject.Pack))
            {
                if (psdObject.Data.TryGetValue("Pack", out String dataPack)) psdObject.Pack = dataPack;
                else psdObject.Pack = parent == null ? _Settings.OutputSceneName : parent.Pack;
            }

            if (String.IsNullOrEmpty(psdObject.Type))
            {
                psdObject.Type = layer.HasImage ? "Sprite" : "Group";
            }

            if (String.IsNullOrEmpty(psdObject.Group))
            {
                psdObject.Group = parent?.Group;
            }

            if (String.IsNullOrEmpty(psdObject.Name))
            {
                String myLayerName = layer.Name;
                myLayerName = myLayerName.Split(new[] { '#' }, StringSplitOptions.RemoveEmptyEntries)[0].TrimEnd();
                psdObject.Name = myLayerName;
            }

            PsdLayer[] psdChildLayers = layer.Childs.ToArray();
            if (_Settings.ReverseLayers)
            {
                psdChildLayers = psdChildLayers.Reverse().ToArray();
            }

            foreach (PsdLayer psdChildLayer in psdChildLayers)
            {
                PsdObject child = MakeObject(psdObject, psdChildLayer);
                if (child != null) psdObject.Children.Add(child);
            }

            #endregion

            _PsdObjectLayers.Add(psdObject, layer);

            return psdObject;
        }

        /// <summary>Outputs the png file, uses the pack and object data to construct the filepath.
        /// Stores the file path inside <see cref="PsdObject.TexturePath"/></summary>
        /// <param name="layer"></param>
        /// <param name="psdObject"></param>
        /// <param name="tier"></param>
        private void OutputPsdObject(PsdObject psdObject, Int32 tier)
        {
            String indent = "";
            for (Int32 i = 0; i < tier; i++) indent += " ";

            if (psdObject.Children.Any())
            {
                tier++;
                Console.Write(indent + $"[{psdObject.Name}] Object detected\n");
            }
            else if (_PsdObjectLayers.ContainsKey(psdObject) && CreateBitMap(psdObject, _PsdObjectLayers[psdObject], out String texturePath))
            {
                texturePath = texturePath.Replace("\\\\", "\\");
                texturePath = texturePath.Replace("\\", "/");
                texturePath = texturePath.Replace("//", "/");

                String fullPath = texturePath;

                psdObject.TexturePath = texturePath.TrimStart(_Settings.OutputDirectory.ToCharArray());

                //http://www.imagemagick.org/discourse-server/viewtopic.php?t=11103
                //Trim only trims the outside pixel colour inwards until it finds a different pixel colour
                //Add a 1 pixel border of transparent so that it always starts with invis colour first.
                using (MagickImage borderImage = new MagickImage(fullPath))
                {
                    borderImage.BorderColor = new MagickColor(0, 0, 0, 0);
                    borderImage.Border(1, 1);
                    borderImage.Write(fullPath);
                }

                using (MagickImage image = new MagickImage(fullPath, new MagickReadSettings(new PngReadDefines())
                {
                    Width = psdObject.Size[0],
                    Height = psdObject.Size[1],
                }))
                {
                    if (_Settings.TrimOutput)
                    {
                        Int32[] currentSize = { image.Width, image.Height };
                        Int32[] trimmedSize = { image.Width, image.Height };

                        image.Trim();

                        Int32[] sizeDif = { currentSize[0] - trimmedSize[0], currentSize[1] - trimmedSize[1] };
                        psdObject.Offset = new[]
                        {
                            psdObject.Offset[0] + sizeDif[0],
                            psdObject.Offset[1] + sizeDif[1]
                        };
                    }

                    image.Write(fullPath);
                }


                String outputString = indent;

                if (psdObject.Parent != null && !"Pack".Equals(psdObject.Parent.Type))
                {
                    outputString += $"[{psdObject.Parent.Name}] ";
                }
                outputString += $"Created {(String.IsNullOrEmpty(psdObject.Type) ? psdObject.Type : "Sprite")} at location: ";
                outputString += $"{psdObject.TexturePath.Replace(_Settings.OutputDirectory, "..")}";

                Console.WriteLine(outputString);
            }

            if (psdObject.Parent != null)
            {
                if (!"Pack".Equals(psdObject.Parent.Type) && _Settings.PrefixParentName)
                {
                    psdObject.Name = $"{psdObject.Parent.Name}-{psdObject.Name}";
                }
            }

            foreach (PsdObject psdObjectChild in psdObject.Children)
            {
                OutputPsdObject(psdObjectChild, tier);
            }
        }

        /// <summary>Attempt to create a PNG output file using the data from a given <see cref="IPsdLayer"/></summary>
        /// <param name="psdObject"></param>
        /// <param name="layer"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private Boolean CreateBitMap(PsdObject psdObject, IPsdLayer layer, out String filePath)
        {
            filePath = null;
            if (layer == null || !layer.HasImage) return false;

            //Only create png's for Sprites.
            if (psdObject.Type != null && !"sprite".Equals(psdObject.Type.ToLowerInvariant())) return false;

            BitmapSource bitmap = layer.GetBitmap();

            StringBuilder graphicsPathBuilder = new StringBuilder();
            graphicsPathBuilder.Append(_Settings.OutputGraphicsDirectory);
            graphicsPathBuilder.Append("\\");
            graphicsPathBuilder.Append(psdObject.Pack);
            graphicsPathBuilder.Append("\\");

            graphicsPathBuilder.Append($"{HydraExporter.SceneName}");
            graphicsPathBuilder.Append("\\");

            //Grabs the path from the parents
            List<String> parentPath = new List<String>();
            PsdObject currentParent = psdObject.Parent;
            while (currentParent != null)
            {
                //Ignore groups in the texture path called Sprites, these are just collections of sprites with the same data
                if ("Sprites" != currentParent.Group)
                {
                    parentPath.Add(currentParent.Group);
                }
                currentParent = currentParent.Parent;
            }

            //Reverse the path to ensure you are going out from yourself
            parentPath.Reverse();
            foreach (String parentGroup in parentPath)
            {
                graphicsPathBuilder.Append(parentGroup);
                graphicsPathBuilder.Append("\\");
            }


            String psdName = psdObject.Name;
            //Trims any hash value trailing the actual name. This allows duplicate names
            //eg, "ColourfulPencil_Yellow #1" and "ColourfulPenci_Yellow #2"
            psdName = psdName.Split(new[] { '#' }, StringSplitOptions.RemoveEmptyEntries)[0].TrimEnd();

            //Also trims @ as this identifies a sprite layer to be ignored for XML generation

            psdName = Regex.Replace(psdName, @"\t|\n|\r|@|#", "");

            graphicsPathBuilder.Append(psdName);

            graphicsPathBuilder.Append(".png");

            filePath = graphicsPathBuilder.ToString();

            FileUtility.Prepare(filePath);
            using (FileStream stream = new FileStream(filePath, FileMode.Create))
            {
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitmap));
                encoder.Save(stream);
            }

            return true;
        }
    }

    public static class BitMapExt
    {
        public static BitmapSource GetBitmap(this IImageSource imageSource)
        {
            if (imageSource.HasImage == false)
                return null;

            Byte[] data = imageSource.MergeChannels();
            Int32 channelCount = imageSource.Channels.Length;
            Int32 pitch = imageSource.Width * imageSource.Channels.Length;
            Int32 w = imageSource.Width;
            Int32 h = imageSource.Height;

            //var format = channelCount == 3 ? TextureFormat.RGB24 : TextureFormat.ARGB32;
            //var tex = new Texture2D(w, h, format, false);
            Color[] colors = new Color[data.Length / channelCount];


            Int32 k = 0;
            for (Int32 y = h - 1; y >= 0; --y)
            {
                for (Int32 x = 0; x < pitch; x += channelCount)
                {
                    Int32 n = x + y * pitch;

                    Color c = Color.FromArgb(1, 1, 1, 1);
                    if (channelCount == 4)
                    {
                        c.B = data[n++];
                        c.G = data[n++];
                        c.R = data[n++];
                        c.A = (Byte)System.Math.Round(data[n] / 255f * imageSource.Opacity * 255f);
                    }
                    else
                    {
                        c.B = data[n++];
                        c.G = data[n++];
                        c.R = data[n];
                        c.A = (Byte)System.Math.Round(imageSource.Opacity * 255f);
                    }

                    colors[k++] = c;
                }
            }

            if (channelCount == 4)
                return BitmapSource.Create(imageSource.Width, imageSource.Height, 96, 96, PixelFormats.Bgra32, null,
                    data, pitch);
            return BitmapSource.Create(imageSource.Width, imageSource.Height, 96, 96, PixelFormats.Bgr24, null,
                data, pitch);
        }
    }
}
