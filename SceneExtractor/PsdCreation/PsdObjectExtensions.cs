﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SceneExtractor.PsdCreation
{
    public static class PsdObjectExtensions
    {
        /// <summary>Will search for the data inside "[ ]" inside the given string.
        /// If any is present, will out the string and return true</summary>
        /// <param name="layerData"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static Boolean GetData(this String layerData, String key, out String data)
        {
            data = null;

            Boolean f = false;
            StringBuilder b = new StringBuilder();
            for (Int32 i = 0; i < layerData.Trim().Length; i++)
            {
                Char c = layerData.Trim()[i];
                //Found the starting tag
                if (c == '[')
                {
                    if (!f) f = true;
                    else f = false;
                    continue;
                }

                //Found end of section
                if (c == ']')
                {
                    //Starting tag still active, record data
                    if (f)
                    {
                        f = false;

                        String dataFound = b.ToString();
                        b.Clear();

                        String[] splitData = dataFound.Split(':');

                        if (splitData.Length == 1)
                        {
                            data = splitData[0];
                            if (key == "" || key == data)
                            {
                                return true;
                            }
                            continue;
                        }


                        String foundKey = splitData[0];
                        String foundData = dataFound.Remove(0, $"{foundKey}:".Length);

                        if (foundKey.Equals(key))
                        {
                            data = foundData;
                            return true;
                        }

                        continue;
                    }

                    data = null;
                    return false;
                }

                if (f)
                {
                    b.Append(c);
                }
            }

            data = null;
            return false;
        }

        public static Dictionary<String, String> GetAllData(this String layerName)
        {
            Dictionary<String, String> data = new Dictionary<String, String>();

            Boolean f = false;
            StringBuilder b = new StringBuilder();
            for (Int32 i = 0; i < layerName.Trim().Length; i++)
            {
                Char c = layerName.Trim()[i];
                //Found the starting tag
                if (c == '[')
                {
                    if (!f) f = true;
                    else f = false;
                    continue;
                }

                //Found end of section
                if (c == ']')
                {
                    //Starting tag still active, record data
                    if (f)
                    {
                        f = false;

                        String dataFound = b.ToString();
                        b.Clear();

                        String[] splitData = dataFound.Split(':');
                        String foundKey = splitData[0];
                        String foundData = splitData.Length > 1 ? dataFound.Remove(0, $"{foundKey}:".Length) : null;

                        data.Add(foundKey, foundData);
                    }
                }

                if (f)
                {
                    b.Append(c);
                }
            }

            return data;
        }
    }
}
