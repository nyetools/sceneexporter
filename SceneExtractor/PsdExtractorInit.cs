﻿using Newtonsoft.Json;
using PsdExporter;
using SceneExtractor.Finder;
using SceneExtractor.Plugins;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace SceneExtractor
{
    public partial class PsdExtractor
    {
        private StartUpArguments _StartUpArguments;

        private void Initialise()
        {
            _Settings = new ExportSettings();
            _StartUpArguments = JsonConvert.DeserializeObject<StartUpArguments>(File.ReadAllText(_PathToArgumentJson[0]));

            _Settings.CustomArguments = _StartUpArguments.CustomArguments.ToDictionary(k => k.Key, v => v.Value);

            //The name of the Scene these graphics are a part of
            //If none given, uses the psdFileName instead
            string sceneName = _StartUpArguments.SceneName;
            _Settings.OutputSceneName = String.IsNullOrEmpty(sceneName) ? _Settings.InputPsdFileName?.Split('.').First() ?? "Scene" : sceneName;

            string fileName = _StartUpArguments.FileName;
            _Settings.OutputFileName = String.IsNullOrEmpty(fileName) ? _Settings.OutputSceneName : fileName;

            string packName = _StartUpArguments.PackName;
            _Settings.PackName = String.IsNullOrEmpty(packName) ? _Settings.OutputSceneName : packName;

            _Settings.IgnoreInvisbleLayers = _StartUpArguments.IgnoreInvisibleLayers;

            //The name of the .psd file being read in
            String psdPath = _StartUpArguments.PSDPath;

            if (psdPath == null)
            {
                OnWriteLine?.Invoke("No path for psd file given, attempting to find psd file.");

                Assembly assembly = Assembly.GetAssembly(typeof(PsdExtractor));
                String directory = Path.GetDirectoryName(assembly.Location);
                DirectoryInfo d = new DirectoryInfo(directory);

                if (!FindPsdFiles.Find(d, _Settings.InputPsdFileName, out String psdFilePath))
                {
                    OnWriteLine?.Invoke($"Cannot find any .psd files");
                    return;
                }

                psdPath = psdFilePath;
                OnWriteLine?.Invoke("");
            }

            _Settings.PsdFilePath = psdPath;
            _Settings.InputPsdFileName = _Settings.PsdFilePath.Split('\\').Last();
            _Settings.InputDirectory = _Settings.PsdFilePath.TrimEnd(_Settings.InputPsdFileName.ToCharArray());

            String outputDirectory = _StartUpArguments.OutputDirectory;
            if (outputDirectory == null)
            {
                Assembly assembly = Assembly.GetAssembly(typeof(PsdExtractor));
                String directory = Path.GetDirectoryName(assembly.Location);

                _Settings.OutputDirectory = $"{directory}";
                OnWriteLine?.Invoke($"No OutputDirectory given");
            }
            else
            {
                _Settings.OutputDirectory = outputDirectory;
                OnWriteLine?.Invoke($"Output folder: {_Settings.OutputDirectory}\n");
            }

            _Settings.OutputDirectory = _Settings.OutputDirectory.Replace("\\\\", "\\");
            _Settings.OutputDirectory = _Settings.OutputDirectory.Replace("\\", "/");

            _Settings.OutputGraphicsDirectory = $"{_Settings.OutputDirectory}/Graphics";
            _Settings.OutputXMLDirectory = $"{_Settings.OutputDirectory}/XML";

            //TODO Should delete old exported files in this location?
            Directory.CreateDirectory(_Settings.OutputGraphicsDirectory);
            Directory.CreateDirectory(_Settings.OutputXMLDirectory);

            //Get config for reversing children when parsing
            _Settings.ReverseLayers = _StartUpArguments.ReverseChildOrder;

            _Settings.PrefixParentName = _StartUpArguments.ParentPrefixOnNames;
        }

        private Type LoadPlugin()
        {
            String requiredExporterName = _StartUpArguments.ExporterName ?? ".xml";

            String pluginFolder = _StartUpArguments.PluginDirectory ?? Path.Combine(Environment.CurrentDirectory, "Plugins");

            Type[] plugins = PluginLoader.Load(pluginFolder);

            Type exporterType = plugins.FirstOrDefault(s => requiredExporterName.Equals(s.GetCustomAttribute<ExportTypeAttribute>()?.HumanName));

            return exporterType;
        }
    }
}
