﻿using System;
using System.IO;
using System.Linq;

namespace SceneExtractor.Finder
{
    public class FindPsdFiles
    {
        public static Boolean Find(DirectoryInfo directoryInfo, String psdName, out String psdFilePath)
        {
            psdFilePath = null;

            Console.WriteLine($"Searching {directoryInfo.FullName} ...");
            FileInfo[] psdFiles = directoryInfo.GetFiles("*.psd");

            if (psdFiles.Length > 0)
            {
                Console.WriteLine($"\".psd\" found, in {directoryInfo.FullName}:");
                for (Int32 i = 0; i < psdFiles.Length; i++)
                {
                    FileInfo fileInfo = psdFiles[i];
                    Console.WriteLine($"{i + 1}: {fileInfo.FullName}");
                }

                FileInfo nameMatchPath = psdFiles.FirstOrDefault(s => s.FullName.EndsWith($"{psdName}.psd"));
                if (nameMatchPath != null)
                {
                    Console.WriteLine($"Selected: {nameMatchPath.FullName}");
                    psdFilePath = nameMatchPath.FullName;
                }
                else
                {
                    Console.WriteLine($"Selecting {psdFiles.First().FullName}\n");
                    psdFilePath = psdFiles.First().FullName;
                }
            }
            else
            {
                return false;
            }

            return true;
        }
    }
}
