﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Ntreev.Library.IO;
using PsdExporter;

namespace SceneExtractor.Plugins
{
    public static class PluginLoader
    {
        private static String PluginFolder => $"{new DirectoryInfo(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location))}\\Plugins\\";

        private static List<Type> _Plugins = new List<Type>();

        public static Type[] Load(String pluginDirectory = null)
        {
            if (pluginDirectory == null)
            {
                pluginDirectory = PluginFolder;
            }

            FileUtility.Prepare(pluginDirectory);

            DirectoryInfo pluginFolder = new DirectoryInfo(pluginDirectory);
            FileInfo[] possibleDlls = pluginFolder.GetFiles("*.dll");

            foreach (FileInfo possibleDll in possibleDlls)
            {
                try
                {
                    Assembly dlAssembly = Assembly.LoadFile(possibleDll.FullName);

                    foreach (TypeInfo dlAssemblyDefinedType in dlAssembly.DefinedTypes.Where(s => s.BaseType == typeof(Exporter)))
                    {
                        _Plugins.Add(dlAssemblyDefinedType.AsType());
                    }
                }
                catch (Exception e)
                {

                }
            }

            return _Plugins.ToArray();
        }
    }
}
